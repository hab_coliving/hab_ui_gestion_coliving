import React, {useEffect, useState} from 'react'
import { useSelector, useDispatch } from 'react-redux'

const useServices = () => {
    const dispatch = useDispatch();
    const token = useSelector(s => s.user && s.user.token);
    const services = useSelector(s => s.services);

    useEffect(() => {
    const setServices = services => dispatch({ type: 'services', services});
    fetch('http://localhost:3300/user/services', {
        headers: {
            'Authorization': 'Bearer ' + token
            }
        }).then(res => res.json())
        .then(data => setServices(data))
        .catch(() => alert('Error al cargar los servicios...'))
}, [token, dispatch]);
  return services;
}

const Services = () => {
    const services = useServices();
    let message;
    if (services.length === 0) {
        message = 
        <div>
            <h2>Tus servicios</h2>
            <p className="listservices">Actualmente no tienes ningún servicio contratado</p>
        </div>
    } else {
        message = <h2>Servicios incluidos en tu reserva actual:</h2>
    }

    return (
      <div className="user-services">
        {message}
          <ul>
              {services && services.map(service =>
                  <li>
                      {service.name}
                  </li>
              )}
          </ul>
      </div>
    )
}

export default Services
