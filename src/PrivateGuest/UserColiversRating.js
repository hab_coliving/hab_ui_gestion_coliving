import React, { useState } from 'react'
import { useSelector } from 'react-redux'

const RatingColiver = () => {
    const user = useSelector(s => s.user);
    const token = useSelector(s => s.user && s.user.token);
    const coliving = useSelector(s => s.coliving);
    const [scored, setScored] = useState ('');
    const [score, setScore] = useState('score');
    const handleColiverScore = e => setScore(e.target.value);
    const handleScored = e => setScored(e.target.value);
    const [isError, setError] = useState(false);
    const [status, setStatus] = useState ();

   
    const handleSubmit = async (e) => {
        e.preventDefault();
        const rate = { score, coliving, scored, user };
        setError(false);
        try {
            const result = await fetch('http://localhost:3300/user/coliver/rate', {
                method: 'POST',
                body: JSON.stringify(rate),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            });
            if (result.status === 200)
                setStatus("Usuario valorado!");
            else
                alert("Error valorando usuario. Comprueba que no haya sido ya valorado");

            setScored("");
        } catch (error) {
            console.warn('Error:', error);
            setError(true);
        }
    };

    return (
        <form id="coliver-rating" onSubmit={handleSubmit}>
            <h2>Puntúa a tus compañeros</h2>
            <div className="scorer-elements">
                <label>
                    Email coliver:
                    <input className="scored-item" name="scored" required value={scored} onChange={handleScored}/>

                </label>
            </div>
            <div className="scorer-elements">
              <label>
                    Puntuación:
                <select className="score" name="score" size="1" onChange={handleColiverScore} >
                    <option value="1">1 estrella</option>
                    <option value="2">2 estrellas</option>
                    <option value="3">3 estrellas</option>
                    <option value="4">4 estrellas</option>
                    <option value="5">5 estrellas</option>
                </select>
              </label>
            </div>
            <div className="doscore">
            <button className="save-score">Enviar</button>
            {isError && <div>Error, inténtalo de nuevo</div>}
            </div>
            <p className="okbooked">{status}</p>
        </form>
    )
};

export default RatingColiver
