import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import Moment from 'moment';


const RatingColiving = () => {
    const user = useSelector(s => s.user);
    const token = useSelector(s => s.user && s.user.token);
    const coliving = useSelector(s => s.coliving);
    const [colivingScore, setColivingScore] = useState('colivingScore');
    const [reservationId, setReservationId] = useState('reservationId');
    const handleColivingScore = e => setColivingScore(e.target.value);
    const handleReservationId = e => setReservationId(e.target.value);
    const [isError, setError] = useState(false);
    const [reservations, setReservations] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        if (!loading)
            return;
        fetch('http://localhost:3300/user/reservations/', {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        })
            .then(response => response.json())
            .then(jsonResponse => {
                setReservations(jsonResponse);
                setLoading(false);
            });
    });

    const handleSubmit = async (e) => {
        e.preventDefault();
        const rating = { reservationId, colivingScore, coliving, user };
        setError(false);
        try {
            const result = await fetch('http://localhost:3300/user/rating-coliving', {
                method: 'POST',
                body: JSON.stringify(rating),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                }
            });
            if (result.status === 200)
                alert("Coliving valorado");
            else
                alert("Se ha producido un error al valorar el coliving");
        } catch (err) {
            console.warn('Error:', err);
            setError(true)
        }
    };

    return (
        <div className="host-profile">
            <h2 className="score-title">Puntúa tus {reservations.length} estancias.</h2>
            <ul className="bookingslist">
            {
                loading ? (
                    <span>loading...</span>
                ) : (
                    reservations.map(reservation => (
                        <li className="bookinglist-item">Reserva: {reservation.reservationid}, Apartamento: {reservation.id} - {Moment(reservation.entrydate).format('DD/MM/YYYY')}
                            <form id="coliving-rating" onSubmit={handleSubmit}>
                                <div className="scorer-elements">
                                    <label>
                                        Puntuación:
                                        <select className="score" name={reservation.reservationid} value={colivingScore} size="1" onChange={handleColivingScore} >
                                            <option value="1">1 estrella</option>
                                            <option value="2">2 estrellas</option>
                                            <option value="3">3 estrellas</option>
                                            <option value="4">4 estrellas</option>
                                            <option value="5">5 estrellas</option>
                                        </select>
                                    </label>
                                </div>
                                <input type='hidden' name="score" value={reservation.reservationid}/>
                                <button className="save-coliving-score">Enviar</button>
                            </form>
                        </li>
                    ))
                )
            }
            </ul>
        </div>

    )
};

export default RatingColiving


