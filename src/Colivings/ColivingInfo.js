import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { FaStar } from "react-icons/fa"
import Apartment from "./Apartment";
import './ColivingInfo.css';

const getColiving = id =>
    fetch(`http://localhost:3300/coliving/${id}`)
        .then(r => r.json());

const ColivingInfo = () => {
    const [loading, setLoading] = useState(true);
    const [apartments, setApartments] = useState([]);
    const [errorMessage, setErrorMessage] = useState(null);
    const { id } = useParams();

    const [coliving, setColiving] = useState();
    useEffect(() => {
        fetch(`http://localhost:3300/apartments/${id}`)
            .then(response => response.json())
            .then(jsonResponse => {
                setApartments(jsonResponse);
                setLoading(false);
            });

        getColiving(id).then(c => setColiving(c))
    }, [id]);
    if (!coliving) {
        return (
            "loading"
        )
    }

    const divInfoStyle = {
        backgroundImage: `url(${coliving.poster})`,
        backgroundPosition: "center",
        backgroundSize: "cover",
        width: "67.5rem",
        height: "40.8rem",
        opacity: 0.85
    };

    const StarRating = () => {
        const stars = [1, 2, 3, 4, 5]        

            return (
                <div>
                    {stars.map((star, i) => {
                        const value = i + 1;

                        return (
                            <FaStar
                                className="star"
                                index={i}
                                key={star.toString()}
                                value={i + 1}
                                score={coliving.score}
                                color={(value <= coliving.score) ? "#ffc107" : "#afafb1"}
                                size={15}
                            />
                        );
                    })}
                </div>
            );
        };

    return (
        <div className="full-info">
            <div className="coliving-info">
                <div className="coliving-pic">
                    <div className="coliving-info-img" style={divInfoStyle} alt={`${coliving.name} en ${coliving.city}`}></div>
                </div>
                <div className="details">
                    <h2>{coliving.name}</h2>
                    <div className="info">
                        <span className="info-location">{coliving.city}</span>
                        <br />
                        <span className="info-prize">Weekly price: {coliving.weekly_price}€</span>
                        <br />
                        <span className="info-prize">Monthly price: {coliving.monthly_price}€</span>
                        <br />
                        <span className="info-punctuation"><StarRating /></span>
                    </div>
                    <p className="more">{coliving.description}</p>
                </div>
            </div>
            <div className="apartments">
                <h2>Apartamentos disponibles</h2>
                <div className="book-apartment">
                    {
                        loading && !errorMessage ? (
                            <span>loading...</span>
                        ) : errorMessage ? (
                            <div className="errorMessage">{errorMessage}</div>
                        ) : (
                                    apartments.map((apartment, index) => (
                                        <Apartment key={`${index}-${apartment.id}`} apartment={apartment} />
                                    ))
                                )
                    }
                </div>
            </div>
        </div>
    )
};

export default ColivingInfo




