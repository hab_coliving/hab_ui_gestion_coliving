import React from "react";
import { Link } from 'react-router-dom'
import { FaStar } from "react-icons/fa"

const Coliving = ({ coliving }) => {
    const divStyle = {
        backgroundImage: `url(${coliving.poster})`,
        backgroundPosition: "center",
        backgroundSize: "cover",
        width: "34.37rem",
        height: "20.78rem",
        opacity: 0.85
    }

    const StarRating = () => {
        const stars = [1, 2, 3, 4, 5]        

            return (
                <div>
                    {stars.map((star, i) => {
                        const value = i + 1;

                        return (
                            <FaStar
                                className="star"
                                index={i}
                                key={star.toString()}
                                value={i + 1}
                                score={coliving.score}
                                color={(value <= coliving.score) ? "#ffc107" : "#afafb1"}
                                size={15}
                            />
                        );
                    })}
                </div>
            );
        };

        return (
            <div className="coliving-view">
                <div className="colivingimg" style={divStyle} alt={`${coliving.name} en ${coliving.city}`}></div>
                <h2><Link to={`/colivings/${coliving.id}`}>{coliving.name}</Link></h2>
                <div className="location-stars">
                    <span className="location">{coliving.city} <span className="starrating"><StarRating /></span> </span>
                </div>
                <div className="weekly-prize">
                    <span className="prize">Weekly price: {coliving.weekly_price}€</span>
                    <br />
                    <span className="prize">Monthly price: {coliving.monthly_price}€</span>
                </div>
            </div>
        )
    }

    export default Coliving

