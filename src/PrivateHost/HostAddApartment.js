import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import Radios from '../Authentication/Radios'
import Apartment from "../Colivings/Apartment";

const useFormField = () => {
    const [value, setValue] = useState('');
    return [value, e => setValue(e.target.value)]
};

const AddApartment = () => {
    //const user = useSelector(s => s.user)
    const coliving = useSelector(s => s.coliving);
    const [apartments, setApartments] = useState([]);
    const [rooms_count, setRooms] = useFormField();
    const [roomType, handleRoomType] = useFormField();
    const [photo_room1, setPhoto] = useFormField({});
    const [loading, setLoading] = useState(true);
    const [errorMessage, setErrorMessage] = useState(null);
    const [status, setStatus] = useState ();

    const [isError, setError] = useState(false);

    useEffect(() => {
        fetch(`http://localhost:3300/apartments/${coliving.id}`)
            .then(response => response.json())
            .then(jsonResponse => {
                setApartments(jsonResponse);
                setLoading(false);
            });
    });

    const handleSubmit = async (e) => {
        e.preventDefault();
        const apartment = { rooms_count, roomType, photo_room1, coliving };
        setError(false);
        try {
            const result = await fetch('http://localhost:3300/host/add/apartment', {
                method: 'POST',
                body: JSON.stringify(apartment),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + coliving.token
                }
            });
            if (result.status === 200)
                setStatus("Apartamento añadido");
                //setApartments(data)
        } catch (err) {
            console.warn('Error:', err);
            setError(true)
        }
    };

    return (
        <form id="add-apartment" onSubmit={handleSubmit}>
            <h2>Añadir apartamento</h2>
            <div className="add-space">
                <label >
                    Nº de habitaciones:
                    <input className="add-options" type="number" name="rooms_count" min="1" max="10" required value={rooms_count} onChange={setRooms} />
                </label>
            </div>
            <div className="add-space">
                <Radios className="radio-item" name="roomType" options={["compartidas", "privadas", "ambas"]} value={roomType} onChange={handleRoomType} />
            </div>
            <div className="add-space">
                <label>
                    Foto:
                    <input className="add-options" name="photo_room1" value={photo_room1} onChange={setPhoto} placeholder="url imagen" />
                </label>
            </div >         
            <button className="save-apartment">Añadir</button>
            {isError && <div>Error, inténtalo de nuevo</div>}
            <br/><br/><br/>
            <div className="apartments">
                <h2>Apartamentos disponibles</h2>
                <div className="book-apartment">
                    {
                        loading && !errorMessage ? (
                            <span>loading...</span>
                        ) : errorMessage ? (
                            <div className="errorMessage">{errorMessage}</div>
                        ) : (
                            apartments.map((apartment, index) => (
                                <Apartment key={`${index}-${apartment.id}`} apartment={apartment} />
                            ))
                        )
                    }
                </div>
            </div>
            <p className="okbooked">{status}</p>
        </form>
    )
}

export default AddApartment


