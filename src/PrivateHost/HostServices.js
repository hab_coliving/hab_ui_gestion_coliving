import React, {useEffect} from 'react'
import { useSelector, useDispatch } from 'react-redux'

const useHostServices = () => {
  const dispatch = useDispatch();
  const token = useSelector(s => s.coliving && s.coliving.token);
  const services = useSelector(s => s.services);

  useEffect(() => {
    const setHostServices = services => dispatch({ type: 'services', services })
    fetch('http://localhost:3300/host/coliving/services', {
      headers: {
        'Authorization': 'Bearer ' + token
      }
    })
        .then(res => res.json())
        .then(data => setHostServices(data))
        .catch(() => alert('Error al cargar los servicios...'))
  }, [token, dispatch]);

  return services
};

const HostServices = () => {
  const services = useHostServices();

  if (!services)
    return 'Cargando ...';

  return (
      <div className="host-profile">
        <h2>Servicios del coliving</h2>
        <p>Servicios ofertados: {services.length}</p>
        <ul>
          {services.map(service =>
              <li>
                {service.name}
              </li>
          )}
        </ul>
      </div>
  )
};

export default HostServices
