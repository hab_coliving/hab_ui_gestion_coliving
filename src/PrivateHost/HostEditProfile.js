
import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Radios from '../Authentication/Radios'

const HostEditProfile = () => {
    const dispatch = useDispatch();
    const editprofile = (coliving) => dispatch({ type: 'editprofileHost', coliving });
    const coliving = useSelector(s => s.coliving);
    const [data, setData] = useState(coliving);
    const [status, setStatus] = useState();

    if (!data) return 'Loading...';

    const handleField = field => e => setData({ ...data, [field]: e.target.value });

    const handleSave = async (e) => {
        e.preventDefault();
        try {
            const result = await fetch('http://localhost:3300/host/dashboard/profile/edit', {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + coliving.token
                },
                body: JSON.stringify(data)
            });
            if (result.status === 200)
                setStatus('Perfil editado correctamente!');
            else
                alert("Se ha producido un error al modificar el perfil");
        } catch (error) {
            console.warn('Error:', error);
            alert("Se ha producido un error al modificar el perfil");
        }
    };


    return (
        <form className="host-edit-profile" onSubmit={handleSave}>
            <h2>Editar perfil</h2>
            <div className="host-profile-wrap">
                <div className="profile-info">
                    <div className="profile-elements">
                        <input className="profile-item" type="hidden" name="id" value={data.id || ''} onChange={handleField('id')} />
                    </div>
                    <div className="profile-elements">
                        <label>
                            Nombre:
                <input className="profile-item" name="contact_name" value={data.contact_name || ''} onChange={handleField('contact_name')} />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            Apellido:
                <input className="profile-item" name="contact_surname" value={data.contact_surname || ''} onChange={handleField('contact_surname')} />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            Email:
                <input className="profile-item" type="email" name="contact_email" value={data.contact_email || ''} onChange={handleField('contact_email')} />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            Coliving:
                <input className="profile-item" name="name" value={data.name || ''} onChange={handleField('name')} />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            Calle:
                <input className="profile-item" name="street" value={data.street || ''} onChange={handleField('street')} />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            C.P:
                <input className="profile-item" name="zip_code" value={data.zip_code || ''} onChange={handleField('zip_code')} />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            País:
                <input className="profile-item" name="country" value={data.country || ''} onChange={handleField('country')} />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            Nº de apartamentos:
                <input className="profile-item" name="apartments_count" type="number" min="1" max="25" value={data.apartments_count || ''} onChange={handleField('apartments_count')} />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            Nº total de habitaciones:
                <input className="profile-item" name="rooms_count" type="number" min="1" max="100" value={data.rooms_count || ''} onChange={handleField('rooms_count')} />
                        </label>
                    </div>
                    <div className="profile-elements radios">
                        <label>
                            <Radios className="radio-item" name="room_type" options={["compartidas", "privadas", "ambas"]} value={data.rooms_type || ''}
                                onChange={handleField('room_type')} />
                        </label>
                    </div>
                </div>

                <div className="profile-info">
                    <div className="profile-elements">
                        <label>
                            Servicios:
                <input className="profile-item" name="services" value={data.services || ''} onChange={handleField('services')} />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            €/semana:
                <input className="profile-item" name="weekly_price" type="number" min="150€" max="800€" value={data.weekly_price || ''} onChange={handleField('weekly_price')} />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            €/mes:
                <input className="profile-item" type="number" name="monthly_price" min="600€" max="2500€" value={data.monthly_price || ''} onChange={handleField('monthly_price')} />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            Foto:
                <input className="profile-item" name="poster" type="url" value={data.poster || ''} onChange={handleField('poster')} placeholder="url imagen" />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            <textarea className="text-profile-item" name="description" value={data.description || ''} onChange={handleField('description')} placeholder="Descripción" cols="35" rows="5" />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            Web:
                <input className="profile-item" type="url" name="website" value={data.website || ''} onChange={handleField('website')} />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            Facebook:
                <input className="profile-item" type="url" name="facebook" value={data.facebook || ''} onChange={handleField('facebook')} />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            Instagram:
                <input className="profile-item" type="url" name="instagram" value={data.instagram || ''} onChange={handleField('instagram')} />
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            Twitter:
                <input className="profile-item" type="url" name="twitter" value={data.twitter || ''} onChange={handleField('twitter')} />
                        </label>
                    </div>

                    <button className="save-profile host">Guardar</button>
                    <p className="okbooked">{status}</p>
                </div>
            </div>
        </form>
    )
};

export default HostEditProfile

