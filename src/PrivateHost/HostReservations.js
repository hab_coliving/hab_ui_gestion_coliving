import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Moment from 'moment';

const useHostReservations = () => {
  const dispatch = useDispatch();
  const token = useSelector(s => s.coliving && s.coliving.token);
  const reservations = useSelector(s => s.reservations);
  useEffect(() => {
    const setHostReservations = reservations => dispatch({ type: 'reservations', reservations});
    fetch('http://localhost:3300/host/coliving/reservations', {

      headers: {
        'Authorization': 'Bearer ' + token
      }
    })
      .then(res => res.json())
      .then(data => setHostReservations(data))
      .catch(() => alert('Error al cargar las reservas...'))
}, [token, dispatch]);
  return reservations
};

const HostReservations = () => {
  const reservations = useHostReservations();

  if (!reservations)
    return 'Cargando ...';

  return (
      <div className="host-profile">
        <h2>Tus reservas</h2>
        <p>Número de reservas: {reservations.length} </p>
    <ul>
      {
        !reservations ? (
            <span>Cargando . .) .</span>
            ) : (
        reservations.map(reservation =>
            <li>
              <span>De {Moment(reservation.entry_date).format('DD/MM/YYYY')} a {Moment(reservation.departure_date).format('DD/MM/YYYY')} ({reservation.price} Euros)</span>
            </li>
        ))
      }
    </ul>
        </div>
  )
};

export default HostReservations
