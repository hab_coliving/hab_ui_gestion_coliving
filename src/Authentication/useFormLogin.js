import { useState, useEffect } from "react";
import { useHistory } from 'react-router-dom'
import { useDispatch } from 'react-redux'

const useFormLogin = (callback, validate) => {
  const dispatch = useDispatch()
  const history = useHistory()
  const login = (user) => dispatch({ type, user })
  const [type, setType] = useState('login')

  const [values, setValues] = useState({ email: "", password: "" });
  const [errors, setErrors] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleChange = event => {
    const { name, value } = event.target;
    setValues({
      ...values,
      [name]: value
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const user = values
    setErrors(validate(values));
    setIsSubmitting(true);
    try {
      const result = await fetch('http://localhost:3300/' + type, {
        method: 'POST',
        body: JSON.stringify(user),
        headers: {
          'Content-Type': 'application/json'
        }
      });

      const data = await result.json();
      if (result.ok)
        login(data);

        if(type === 'login') {
          history.push(`/user/dashboard`)
        }else {
          history.push(`/host/dashboard`)
        }
    } catch (error) {
      console.log('Error:', error)
    }
  };

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      callback();
    }
  }, [errors, callback, isSubmitting]);

  return {
    handleChange,
    handleSubmit,
    values,
    errors,
    type,
    setType
  };
};

export default useFormLogin;