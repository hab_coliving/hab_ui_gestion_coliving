import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router'
import Address from "./Address";


import useFormHostSignup from "./useFormHostSignup";
import validate from "./validateLogin";

import Radios from './Radios'

const HostSignup = () => {
    const history = useHistory();
    const [services, setServices] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const isLoggedIn = useSelector(s => !!s.coliving);

    useEffect(() => {
        if (isLoggedIn) {
            history.push(`/host/dashboard`)
        }

        fetch(`http://localhost:3300/services/`)
            .then(response => response.json())
            .then(jsonResponse => {
                setServices(jsonResponse);
                setLoading(false);
            });
    }, [isLoggedIn, history]);

    const { handleChange, handleSubmit, values, errors, handleChangeFacility } = useFormHostSignup(
        submit,
        validate
    );

    function submit() {
        console.log("Enviado");
        alert("Registrado correctamente");
    }

    return (
        <form id="signup" onSubmit={handleSubmit} noValidate>
            <h2 className="title-form">SIGN UP</h2>
            <section id="contact-info">
                <label>
                    <h3>Información de contacto</h3>
                    <div>
                        <input className={`${errors.name && "inputError"}`} name="name" required value={values.name}
                            onChange={handleChange} placeholder="Nombre" />
                        {errors.name && <p className="error">{errors.name}</p>}
                    </div>
                    <div>
                        <input className={`${errors.surname && "inputError"}`} name="surname" required value={values.surname}
                            onChange={handleChange} placeholder="Apellido" />
                        {errors.surname && <p className="error">{errors.surname}</p>}
                    </div>
                    <div>
                        <input className={`${errors.email && "inputError"}`} name="email" type="email" required value={values.email}
                            onChange={handleChange} placeholder="Email" />
                        {errors.email && <p className="error">{errors.email}</p>}
                    </div>
                    <div>
                        <input className={`${errors.password && "inputError"}`} name="password" type="password" required value={values.password}
                            onChange={handleChange} placeholder="Password" />
                        {errors.password && <p className="error">{errors.password}</p>}
                    </div>

                </label>
            </section>

            <section id="coliving-info">
                <label>
                    <h3>Información del coliving</h3>
                    <div>
                        <input className={`${errors.colivingngName && "inputError"}`} name="colivingName" required value={values.colivingName}
                            onChange={handleChange} placeholder="Nombre del espacio" />
                        {errors.colivingName && <p className="error">{errors.colivingName}</p>}
                        <div>
                            <input className={`${errors.name && "inputError"}`} name="street" required value={values.street}
                                onChange={handleChange} placeholder="Calle" />
                            {errors.street && <p className="error">{errors.street}</p>}
                        </div>
                        <div>
                            <input className={`${errors.surname && "inputError"}`} name="zipcode" required value={values.zipcode}
                                onChange={handleChange} placeholder="C.P" />
                            {errors.zipcode && <p className="error">{errors.zipcode}</p>}
                        </div>
                        <div>
                            <input className={`${errors.email && "inputError"}`} name="city" type="text" required value={values.city}
                                onChange={handleChange} placeholder="Ciudad" />
                            {errors.city && <p className="error">{errors.city}</p>}
                        </div>
                        <div>
                            <input className={`${errors.password && "inputError"}`} name="country" type="text" required value={values.country}
                                onChange={handleChange} placeholder="País" />
                            {errors.country && <p className="error">{errors.country}</p>}
                        </div>
                        <input className={`${errors.photo && "inputError"}`} name="photo_room1" required value={values.photo_room1}

                            onChange={handleChange} placeholder="url imagen" />
                        {errors.poster && <p className="error">{errors.poster}</p>}
                    </div>
                    <label>
                        <p>Nº de apartamentos</p>
                        <div>
                            <input className={`${errors.apartments && "inputError"}`} type="number" name="apartments" min="1" max="25" required value={values.apartments} onChange={handleChange} />
                            {errors.apartments && <p className="error">{errors.apartments}</p>}
                        </div>
                    </label>
                    <label>
                        <p>Nº total de habitaciones</p>
                        <div>
                            <input className={`${errors.rooms && "inputError"}`} type="number" name="rooms_count" min="5" max="100" required value={handleChangeFacility.rooms}
                                onChange={handleChange} />
                            {errors.rooms && <p className="error">{errors.rooms}</p>}
                        </div>
                    </label>
                    <div>
                        <Radios className={`${errors.roomType && "inputError"}`} name="roomType" options={["compartidas", "privadas", "ambas"]} required value={values.roomType}
                            onChange={handleChange} />
                        {errors.roomType && <p className="error">{errors.roomType}</p>}
                    </div>
                    <div id="services">
                        <p>Servicios incluidos</p>
                        <select multiple size="5" name="facilities" value={values.facilities} onChange={handleChangeFacility} >

                            <optgroup label="Instalaciones y servicios">
                                {
                                    isLoading ? (
                                        <span>loading...</span>
                                    ) : (
                                            services.map((service, index) => (
                                                <option value={`${service.name}`}>{`${service.name}`}</option>
                                            ))
                                        )
                                }
                            </optgroup>
                        </select>
                    </div>
                    <div className="profile-elements">
                        <label>
                            €/semana:
                        <input id="price-item" className={`${errors.weekly_price && "inputError"}`} name="weekly_price" type="number" value={values.weekly_price}
                                onChange={handleChange} />
                            {errors.weekly_price && <p className="error">{errors.weekly_price}</p>}
                        </label>
                    </div>
                    <div className="profile-elements">
                        <label>
                            €/mes:
                        <input id="price-item" className= {`${errors.monthly_price && "inputError"}`} name="monthly_price" type="number" value={values.monthly_price}
                                onChange={handleChange} />
                            {errors.monthly_price && <p className="error">{errors.monthly_price}</p>}
                        </label>
                    </div>
                    <div className="profile-elements">
                        <textarea id="details" className={`${errors.description && "inputError"}`} name="description" value={values.description}
                            onChange={handleChange} placeholder="Descripción" cols="35" rows="5" />
                        {errors.description && <p className="error">{errors.description}</p>}
                    </div>
                    <div>
                        <input className={`${errors.website && "inputError"}`} name="website" type="url" value={values.website}
                            onChange={handleChange} placeholder="Web" />
                        {errors.website && <p className="error">{errors.website}</p>}
                    </div>
                    <div>
                        <input className={`${errors.facebook && "inputError"}`} name="facebook" type="url" value={values.facebook}
                            onChange={handleChange} placeholder="Link Facebook" />
                        {errors.facebook && <p className="error">{errors.facebook}</p>}
                    </div>
                    <div>
                        <input className={`${errors.instagram && "inputError"}`} name="instagram" type="url" value={values.instagram}
                            onChange={handleChange} placeholder="Link Instagram" />
                        {errors.facebook && <p className="error">{errors.facebook}</p>}
                    </div>
                    <div>
                        <input className={`${errors.twitter && "inputError"}`} name="twitter" type="url" value={values.twitter}
                            onChange={handleChange} placeholder="Link Twitter" />
                        {errors.twitter && <p className="error">{errors.twitter}</p>}
                    </div>
                </label>
            </section>
            <button className="doLogin">Registrarse</button>
        </form>
    )
};

export default HostSignup



