import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

import useFormLogin from "./useFormLogin";
import validate from "./validateLogin";
import './Login.css'


const Login = () => {  
    const history = useHistory()
    const handleChangeSelect = e => {
        setType(e.target.value)
    }
    
    const isLoggedIn = useSelector(s => s.user)
    useEffect(() => {
        if (isLoggedIn) {
            history.push(`/user`)
        }
    }, [isLoggedIn, history])

    const { handleChange, handleSubmit, values, errors, type, setType } = useFormLogin(
        submit,
        validate
    );     

    function submit() {
        console.log("Enviado");
    }

    return (
        <form id="login" onSubmit={handleSubmit} noValidate>
            <h2 className="title-form">LOGIN</h2>
            <div>
                <input className={`${errors.email && "inputError"}`} name="email" type="email" required value={values.email}
                    onChange={handleChange} placeholder="Email" />
                {errors.email && <p className="error">{errors.email}</p>}
            </div>
            <div>
                <input className={`${errors.email && "inputError"}`} name="password" type="password" required value={values.password}
                    onChange={handleChange} placeholder="Password" />
                {errors.password && <p className="error">{errors.password}</p>}
            </div>
            <div>
                <select className="typerol" onChange={handleChangeSelect}>
                    <option className="rol" value="login" selected={type === 'login'}>User</option>
                    <option className="rol" value="loginHost" selected={type === 'loginHost'}>host</option>
                </select>
            </div>
            <button className="doLogin">Login</button>

        </form>
    )
}

export default Login


