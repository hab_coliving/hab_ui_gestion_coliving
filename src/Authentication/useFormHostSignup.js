import { useState, useEffect } from "react";
import { useDispatch } from 'react-redux'

const useFormHostLogin = (callback, validate) => {
  const dispatch = useDispatch()
  const login = (user) => dispatch({ type: 'loginHost', user })

  const [values, setValues] = useState({});

  const [errors, setErrors] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);

  const handleChangeFacility = e => {
    const facilities = []
    const options = e.target.options

    for (const option of options) {
        if(option.selected) {
          facilities.push(option.value)
        }
    }

    setValues({ ...values, facilities})
}

  const handleChange = event => {
    const { name, value } = event.target;
    setValues({
      ...values,
      [name]: value
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const coliving = values
    setErrors(validate(values));
    setIsSubmitting(true);
    try {
      const ret = await fetch('http://localhost:3300/host/signup', {
        method: 'POST',
        body: JSON.stringify(coliving),
        headers: {
          'Content-Type': 'application/json'
        }
      })

      const data = await ret.json()

      if(ret.status === 400) {
        alert(data)
      }
      if (ret.status === 200)
        login(data)
    } catch (err) {
      console.log('Error:', err)
    }
};

useEffect(() => {
  if (Object.keys(errors).length === 0 && isSubmitting) {
    callback();
  }
}, [errors, callback, isSubmitting]);

return {
  handleChange,
  handleSubmit,
  values,
  errors,
  handleChangeFacility
};
};

export default useFormHostLogin;